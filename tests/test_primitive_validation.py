import typing
import unittest

from d3m import container, exceptions
from d3m.metadata import hyperparams, base as metadata_base
from d3m.primitive_interfaces import base, transformer

Inputs = container.List[float]
Outputs = container.List[float]


class Hyperparams(hyperparams.Hyperparams):
    pass


class TestPrimitiveValidation(unittest.TestCase):
    def test_multi_produce_missing_argument(self):
        with self.assertRaisesRegex(exceptions.InvalidPrimitiveCodeError, '\'multi_produce\' method arguments have to be an union of all arguments of all produce methods, but it does not accept all expected arguments'):
            class TestPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
                metadata = metadata_base.PrimitiveMetadata({
                    'id': '67568a80-dec2-4597-a10f-39afb13d3b9c',
                    'version': '0.1.0',
                    'name': "Test Primitive",
                    'python_path': 'd3m.primitives.test.TestPrimitive',
                    'algorithm_types': [
                        metadata_base.PrimitiveAlgorithmType.NUMERICAL_METHOD,
                    ],
                    'primitive_family': metadata_base.PrimitiveFamily.OPERATOR,
                })

                def produce(self, *, inputs: Inputs, second_inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                    pass

    def test_multi_produce_extra_argument(self):
        with self.assertRaisesRegex(exceptions.InvalidPrimitiveCodeError, '\'multi_produce\' method arguments have to be an union of all arguments of all produce methods, but it accepts unexpected arguments'):
            class TestPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
                metadata = metadata_base.PrimitiveMetadata({
                    'id': '67568a80-dec2-4597-a10f-39afb13d3b9c',
                    'version': '0.1.0',
                    'name': "Test Primitive",
                    'python_path': 'd3m.primitives.test.TestPrimitive',
                    'algorithm_types': [
                        metadata_base.PrimitiveAlgorithmType.NUMERICAL_METHOD,
                    ],
                    'primitive_family': metadata_base.PrimitiveFamily.OPERATOR,
                })

                def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                    pass

                def multi_produce(self, *, produce_methods: typing.Sequence[str], inputs: Inputs, second_inputs: Inputs, timeout: float = None, iterations: int = None) -> base.MultiCallResult:
                    pass

    def test_produce_using_produce_methods(self):
        with self.assertRaisesRegex(exceptions.InvalidPrimitiveCodeError, 'Produce method cannot use \'produce_methods\' argument'):
            class TestPrimitive(transformer.TransformerPrimitiveBase[Inputs, Outputs, Hyperparams]):
                metadata = metadata_base.PrimitiveMetadata({
                    'id': '67568a80-dec2-4597-a10f-39afb13d3b9c',
                    'version': '0.1.0',
                    'name': "Test Primitive",
                    'python_path': 'd3m.primitives.test.TestPrimitive',
                    'algorithm_types': [
                        metadata_base.PrimitiveAlgorithmType.NUMERICAL_METHOD,
                    ],
                    'primitive_family': metadata_base.PrimitiveFamily.OPERATOR,
                })

                def produce(self, *, inputs: Inputs, produce_methods: typing.Sequence[str], timeout: float = None, iterations: int = None) -> base.CallResult[Outputs]:
                    pass


if __name__ == '__main__':
    unittest.main()
