import builtins
import logging
import typing
import unittest

from d3m import types, utils
from d3m.container import list


class TestUtils(unittest.TestCase):
    def test_get_type_arguments(self):
        A = typing.TypeVar('A')
        B = typing.TypeVar('B')
        C = typing.TypeVar('C')

        class Base(typing.Generic[A, B]):
            pass

        class Foo(Base[A, None]):
            pass

        class Bar(Foo[A], typing.Generic[A, C]):
            pass

        class Baz(Bar[float, int]):
            pass

        self.assertEqual(utils.get_type_arguments(Bar), {
            A: typing.Any,
            B: type(None),
            C: typing.Any,
        })
        self.assertEqual(utils.get_type_arguments(Baz), {
            A: float,
            B: type(None),
            C: int,
        })

        self.assertEqual(utils.get_type_arguments(Base), {
            A: typing.Any,
            B: typing.Any,
        })

        self.assertEqual(utils.get_type_arguments(Base[float, int]), {
            A: float,
            B: int,
        })

        self.assertEqual(utils.get_type_arguments(Foo), {
            A: typing.Any,
            B: type(None),
        })

        self.assertEqual(utils.get_type_arguments(Foo[float]), {
            A: float,
            B: type(None),
        })

    def test_issubclass(self):
        self.assertTrue(utils.is_subclass(list.List[float], types.Container))
        self.assertTrue(utils.is_subclass(list.List[float], list.List))
        self.assertTrue(utils.is_subclass(list.List[float], list.List[typing.Any]))
        self.assertTrue(utils.is_subclass(list.List[float], list.List[float]))
        self.assertFalse(utils.is_subclass(list.List[float], list.List[str]))

        T1 = typing.TypeVar('T1', bound=list.List)
        self.assertTrue(utils.is_subclass(list.List[float], T1))

        T2 = typing.TypeVar('T2', bound=list.List[float])
        self.assertTrue(utils.is_subclass(list.List[float], T2))

        T3 = typing.TypeVar('T3', bound=list.List[str])
        self.assertFalse(utils.is_subclass(list.List[float], T3))

    def test_create_enum(self):
        obj = {
            'definitions': {
                'foobar1':{
                    'type': 'array',
                    'items': {
                        'anyOf':[
                            {'enum': ['AAA']},
                            {'enum': ['BBB']},
                            {'enum': ['CCC']},
                            {'enum': ['DDD']},
                        ],
                    },
                },
                'foobar2': {
                    'type': 'array',
                    'items': {
                        'type': 'object',
                        'anyOf': [
                            {
                                'properties': {
                                    'type': {
                                        'type': 'string',
                                        'enum': ['EEE'],
                                    },
                                },
                            },
                            {
                                'properties': {
                                    'type': {
                                        'type': 'string',
                                        'enum': ['FFF'],
                                    },
                                },
                            },
                            {
                                'properties': {
                                    'type': {
                                        'type': 'string',
                                        'enum': ['GGG'],
                                    },
                                },
                            },
                        ],
                    },
                },
                'foobar3': {
                    'type': 'string',
                    'enum': ['HHH', 'HHH', 'III', 'JJJ'],
                }
            },
        }

        Foobar1 = utils.create_enum_from_json_schema_enum('Foobar1', obj, 'definitions.foobar1.items.anyOf[*].enum[*]')
        Foobar2 = utils.create_enum_from_json_schema_enum('Foobar2', obj, 'definitions.foobar2.items.anyOf[*].properties.type.enum[*]')
        Foobar3 = utils.create_enum_from_json_schema_enum('Foobar3', obj, 'definitions.foobar3.enum[*]')

        self.assertSequenceEqual(builtins.list(Foobar1.__members__.keys()), ['AAA', 'BBB', 'CCC', 'DDD'])
        self.assertSequenceEqual([value.value for value in Foobar1.__members__.values()], [1, 2, 3, 4])

        self.assertSequenceEqual(builtins.list(Foobar2.__members__.keys()), ['EEE', 'FFF', 'GGG'])
        self.assertSequenceEqual([value.value for value in Foobar2.__members__.values()], [1, 2, 3])

        self.assertSequenceEqual(builtins.list(Foobar3.__members__.keys()), ['HHH', 'III', 'JJJ'])
        self.assertSequenceEqual([value.value for value in Foobar3.__members__.values()], [1, 2, 3])

        self.assertTrue(Foobar1.AAA.name == 'AAA')
        self.assertTrue(Foobar1.AAA.value == 1)
        self.assertTrue(Foobar1.AAA == Foobar1.AAA)
        self.assertTrue(Foobar1.AAA == 'AAA')

    def test_redirect(self):
        logger = logging.getLogger('test_logger')
        with self.assertLogs(logger=logger, level=logging.DEBUG) as cm:
            with utils.redirect_to_logging(logger=logger):
                print("Test.")

        self.assertEqual(len(cm.records), 1)
        self.assertEqual(cm.records[0].message, "Test.")


if __name__ == '__main__':
    unittest.main()
